// Twelfth Root of Two
let frequency = 440
// 2.0 ** (1.0 / 12.0)
let twelfth_root = 1.0594630943592953
for (let index = 0; index < 13; index++) {
    console.log(frequency)
music.playTone(frequency, music.beat(BeatFraction.Whole))
    basic.pause(200)
    frequency = frequency * twelfth_root
}
